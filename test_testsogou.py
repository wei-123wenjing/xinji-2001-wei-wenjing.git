import pytest
def setup_function():
 print('---开始计算---')
def teardown_function():
 print('---结束计算---')
def teardown():
 print("---结束测试---")

def add(x,y):
    return x + y
@pytest.mark.hebeu
def test_demo1():
     assert add(0,0) == 0
@pytest.mark.hebeu
def test_demo2():
     assert add(1,3) == 4
@pytest.mark.hebeu
def test_demo3():
     assert add(-2,5) == 3
@pytest.mark.hebeu
def test_demo4():
     assert add(2.3,3.2) == 5.5
@pytest.mark.hebeu
def test_demo5():
    assert add(-99, 99) == 0
